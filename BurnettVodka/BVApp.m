//
//  BVApp.m
//  BurnettVodka
//
//  Created by admin on 7/28/13.
//  Copyright (c) 2013 XenoPsi Media. All rights reserved.
//

#import "BVApp.h"
#import "Recipe.h"


@implementation BVApp

@dynamic currentVersionOfFlavorData;
@dynamic currentVersionOfRecipeData;
@dynamic deviceID;
@dynamic favoriteRecipes;

@end
