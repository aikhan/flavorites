//
//  LazyLoadImage.h
//  YECApp
//
//  Created by Ironman on 17/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HSLazyLoadImage : NSObject {
    
    UIImage *image;
    NSString *fileName;
}

@property (nonatomic, retain) UIImage *image;
@property (nonatomic, readonly) NSString *fileName;

- (id)initWithFileName:(NSString *)imageFileName;

@end
