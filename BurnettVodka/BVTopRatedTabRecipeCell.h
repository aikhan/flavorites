//
//  BVTopRatedTabRecipeCell.h
//  BurnettVodka
//
//  Created by admin on 7/24/13.
//  Copyright (c) 2013 XenoPsi Media. All rights reserved.
//

#import "BVRecipeCell.h"

@interface BVTopRatedTabRecipeCell : BVRecipeCell {
    
    UIImageView *mTopRatingBackgroundCircleImageView;
    UILabel *mTopRatingLabel;
}

- (void)updateCellWithRecipe:(Recipe *)recipe andRatingNumber:(NSInteger)ratingNumber;

@end
